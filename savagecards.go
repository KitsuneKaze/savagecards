package main

import (
	"bytes"
	"fmt"
	"github.com/andersfylling/disgord"
	"github.com/andersfylling/snowflake/v3"
	"github.com/mattn/go-shellwords"
	"gopkg.in/alecthomas/kingpin.v2"
	"log"
	"math/rand"
	"strings"
	"time"
)

type Character struct {
	Name         string `json:"name"`
	Quick        bool   `json:"quick"`
	Draw         int    `json:"draw"`
	SelectedCard Card   `json:"-"`
	DrewCards    []Card `json:"-"`
	Suppressed   bool   `json:"suppressed"`
	IsNPC        bool   `json:"is_npc"`
}

const (
	publicChannel = "506323842193686538"
)

func main() {

	rand.Seed(time.Now().UTC().UnixNano())
	client := disgord.New(&disgord.Config{
		BotToken: "NjIyMzg1MDAwOTc2NzQ0NDY4.XXzJXQ.sdD5nuf33rGBywWgQ6zwEqLH9pM",
		Logger:   disgord.DefaultLogger(false), // optional logging, debug=false
	})
	defer client.StayConnectedUntilInterrupted()

	game, err := NewGame(nil)
	if err != nil {
		log.Fatal(err)
	}
	deck := NewDeck()
	Shuffle(deck)

	client.On(disgord.EvtMessageCreate, func(session disgord.Session, evt *disgord.MessageCreate) {
		msg := evt.Message
		if !strings.HasPrefix(msg.Content, "/sb") {
			return
		}
		command := strings.TrimPrefix(msg.Content, "/sb")
		b := &bytes.Buffer{}
		terminated := false
		app := kingpin.New("savagebot", "a bot to handle savage card initiative")
		app.UsageWriter(b)
		app.ErrorWriter(b)
		app.Terminate(func(int) {
			replyAndDeleteAfterN(session, msg, b.String(), 10)
			terminated = true
		})
		roundCommand := app.Command("round", "deal an initiative round with the current deck and settings")
		toggleCommand := app.Command("toggle", "toggle the suppressed state of a active character")
		readyupCommand := app.Command("ready", "ready up (transfer NPC and Players to Active)")
		readyupMerge := readyupCommand.Flag("merge", "merge the NPC list instead of replacing things").Bool()
		toggleNames := toggleCommand.Arg("name", "the name of the person").Strings()
		setNPCCommand := app.Command("set", "set detail of npcs or players")
		setupPlayerFlag := setNPCCommand.Flag("players", "set players instead of the npc section, this will be persisted automatically").Bool()
		setupDefinitions := setNPCCommand.Arg("definition", "the definition of a character. Each extra section is optional, but would require the ones before.  Of the form name:ally|enemy:<num of cards>:quick").Strings()
		showCommand := app.Command("show", "show the current active definitions")
		shuffleCommand := app.Command("shuffle", "shuffle the deck manually")
		args, _ := shellwords.Parse(command)
		command, err := app.Parse(args)
		if err != nil {
			replyAndDeleteAfterN(session, msg, b.String()+"\n"+err.Error(), 10)
		}

		switch command {
		case readyupCommand.FullCommand():
			if *readyupMerge {
				game.Merge()
			} else {
				game.UpdateActive()
			}
			msg.Reply(session, "I readying up")
		case roundCommand.FullCommand():
			private, public, _ := game.DealRound()
			msg.Reply(session, private)
			sendToChannel(session, publicChannel, public)

		case toggleCommand.FullCommand():
			for _, x := range *toggleNames {
				game.Toggle(x)
			}
			replyAndDeleteAfterN(session, msg, game.ShowActive(), 5)
		case showCommand.FullCommand():
			msg.Reply(session, game.ShowActive())
		case shuffleCommand.FullCommand():
			Shuffle(game.Deck)
			msg.Reply(session, "I shuffled the game deck")
		case setNPCCommand.FullCommand():
			game.ParseNpcCommand(*setupDefinitions)
			replyAndDeleteAfterN(session, msg, game.ShowNPC(), 5)
			log.Println(setupPlayerFlag)

		}

	})

}

func sendToChannel(session disgord.Session, channelID string, message ...interface{}) error {
	s, _ := snowflake.GetSnowflake(channelID)
	channel, err := session.GetChannel(s)
	if err != nil {
		return err
	}
	_, err = channel.SendMsgString(session, fmt.Sprint(message...))
	if err != nil {
		return err
	}
	return nil
}

func replyAndDeleteAfterN(session disgord.Session, msg *disgord.Message, message string, n int) {
	m, err := msg.Reply(session, message)
	if err != nil {
		log.Println(err)
		return
	}
	go func() {
		time.Sleep(time.Duration(n) * time.Second)
		_ = session.DeleteMessage(m.ChannelID, m.ID)
		err = session.DeleteMessage(msg.ChannelID, msg.ID)
		if err != nil {
			log.Println(err)
			return
		}
	}()
}

func avg(x []float64) float64 {
	var a float64
	for _, y := range x {
		a = a + y
	}
	return a / float64(len(x))
}

func CardsHaveJoker(drawnCards []Card) bool {
	for _, c := range drawnCards {
		if c.IsJoker() {
			return true
		}
	}
	return false
}
