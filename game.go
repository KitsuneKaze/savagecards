package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"sort"
	"strconv"
	"strings"
)

type Game struct {
	Deck    Deck
	Players []*Character
	Npcs    []*Character
	Active  []*Character
}

var (
	defaultPlayers = []*Character{
		{
			Quick: false,
			Draw:  2,
			Name:  "Clay",
		},
		{
			Quick: false,
			Draw:  1,
			Name:  "Heath",
			IsNPC: true,
		},
		{
			Quick: false,
			Draw:  1,
			Name:  "Braden",
		},
		{
			Quick: false,
			Draw:  1,
			Name:  "Blair",
		},
	}
)

func (g *Game) DealRound() (string, string, bool) {
	for _, p := range g.Active {
		if p.Suppressed {
			p.DrewCards = []Card{NullCard}
		} else {
			p.DrewCards, g.Deck = DealN(g.Deck, p.Draw)
		}
		p.SelectedCard = BestCard(p.DrewCards)
	}
	for anySelectedLessThenRankWithQuick(rankForQuick, g.Active) {
		for _, p := range g.Active {
			if p.Quick {
				var card Card
				card, g.Deck = Deal(g.Deck)
				p.DrewCards = append(p.DrewCards, card)
				p.SelectedCard = BestCard(p.DrewCards)
			}
		}
	}
	sort.Slice(g.Active, func(i, j int) bool {
		return CardLess(g.Active[i].SelectedCard, g.Active[j].SelectedCard)
	})
	private := &bytes.Buffer{}
	players := &bytes.Buffer{}
	both := io.MultiWriter(private, players)
	joker := false
	for _, p := range g.Active {
		if p.Suppressed {
			continue
		}
		var w io.Writer
		if p.IsNPC {
			w = private
		} else {
			w = both
		}
		joker = joker || CardsHaveJoker(p.DrewCards)
		_, _ = fmt.Fprintf(w, "Name : **%s**\n", p.Name)
		fmt.Fprintf(w, "Card : **%s**\n", p.SelectedCard)
		if len(p.DrewCards) > 1 {
			_, _ = fmt.Fprintf(w, "Cards you got : **%s**\n", p.DrewCards)
		}
		fmt.Fprintln(w, "==================================")
	}
	if joker {
		fmt.Fprintln(both, "**Joker!!!**, I'm reshuffling the Deck")
		g.Deck = NewDeck()
		Shuffle(g.Deck)
	}
	fmt.Fprintln(both)
	fmt.Fprintln(both, "---------END-ROUND-------------")
	fmt.Fprintln(both)
	return private.String(), players.String(), joker
}

func (g *Game) UpdateActive() {
	a := make([]*Character, 0, len(g.Players)+len(g.Npcs))
	a = append(a, g.Players...)
	a = append(a, g.Npcs...)
	g.Active = a
}

func (g *Game) Merge() {
	g.Active = append(g.Active,g.Npcs...)
}

func (g *Game) ParseNpcCommand(command []string) error {
	var formedNPC []*Character
	for _, m := range command {
		breakdown := strings.Split(m, ":")
		switch len(breakdown) {
		case 1:
			formedNPC = append(formedNPC, &Character{
				Name:  breakdown[0],
				IsNPC: true,
				Draw:  1,
			})
		case 2:
			formedNPC = append(formedNPC, &Character{
				Name:  breakdown[0],
				IsNPC: !strings.Contains("ally", breakdown[1]),
				Draw:  1,
			})
		case 3:
			roll, err := strconv.Atoi(breakdown[2])
			if err != nil {
				return fmt.Errorf("I didn't understand that :%w", err)
			}
			formedNPC = append(formedNPC, &Character{
				Name:  breakdown[0],
				IsNPC: !strings.Contains("ally", breakdown[1]),
				Draw:  roll,
			})
		case 4:
			roll, err := strconv.Atoi(breakdown[2])
			if err != nil {
				return fmt.Errorf("I didn't understand that :%w", err)
			}
			formedNPC = append(formedNPC, &Character{
				Name:  breakdown[0],
				IsNPC: !strings.Contains("ally", breakdown[1]),
				Draw:  roll,
				Quick: strings.Contains(breakdown[3], "quick"),
			})
		default:
			return fmt.Errorf("I didn't understand %s", command)
		}
	}
	g.Npcs = formedNPC
	return nil
}

func (g *Game) Toggle(name string) error {
	for _, c := range g.Active {
		if strings.EqualFold(name, c.Name) {
			c.Suppressed = !c.Suppressed
			return nil
		}
	}
	return fmt.Errorf("I couldn't find %s", name)
}

func (g *Game) ShowActive() string {
	m , _ :=json.MarshalIndent(g.Active,"", "    ")
	return string(m)
}

func (g *Game) ShowNPC() string {
	m , _ :=json.MarshalIndent(g.Npcs,"", "    ")
	return string(m)
}

func (g *Game) ShowPlayer() string {
	m , _ :=json.MarshalIndent(g.Players,"", "    ")
	return string(m)
}

func anySelectedLessThenRankWithQuick(rank uint8, c []*Character) bool {
	for _, p := range c {
		if !p.Suppressed && p.Quick && p.SelectedCard.Rank < rank {
			return true
		}
	}
	return false
}

func NewGame(r io.Reader) (*Game, error) {
	g := &Game{
		Deck: NewDeck(),
	}
	Shuffle(g.Deck)
	if r != nil {
		var players []*Character
		dec := json.NewDecoder(r)
		err := dec.Decode(&players)
		if err != nil {
			return nil, err
		}
		g.Players = players
		g.Active = make([]*Character, 0, len(g.Players))
		copy(g.Active, g.Players)
	} else {
		g.Players = defaultPlayers
	}
	return g, nil
}
