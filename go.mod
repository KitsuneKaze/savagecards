module gitlab.com/KitsuneKaze/savagecards

go 1.12

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20190924025748-f65c72e2690d // indirect
	github.com/andersfylling/disgord v0.11.3
	github.com/andersfylling/snowflake/v3 v3.0.2
	github.com/mattn/go-shellwords v1.0.6
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
