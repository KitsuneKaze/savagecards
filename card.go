package main

import (
	"math/rand"
	"sort"
)

type Card struct {
	Suit rune
	Rank uint8
}

const (
	rankForQuick = 5
)

var(
	NullCard =  Card{
		Suit: 'n',
		Rank: 0,
	}
)

func (c Card) IsJoker() bool {
	return c.Suit == 'b' || c.Suit == 'r'
}

type Deck []Card

func (c Card) String() string {
	var suit string
	var rank string
	switch c.Suit {
	case 's':
		suit = "Spades"
	case 'd':
		suit = "Diamonds"
	case 'c':
		suit = "Clubs"
	case 'h':
		suit = "Hearts"
	case 'r':
		return "Red Joker"
	case 'b':
		return "Black Jocker"
	case 'n' :
		return "Null Card"
	default:
		return "What?"
	}
	ranks := []string{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"}
	if c.Rank > 13 {
		return "What?"
	}
	rank = ranks[c.Rank]
	return rank + " of " + suit
}

func rankOrder(c Card) int {
	rankOrder := []rune{'r', 'b', 's', 'h', 'd', 'c','n'}
	for i, r := range rankOrder {
		if c.Suit == r {
			return i
		}
	}
	return -1
}

func CardLess(left, right Card) bool {
	if left.Rank != right.Rank {
		return left.Rank > right.Rank
	}
	return rankOrder(left) < rankOrder((right))
}

func BestCard(c []Card) Card {
	sort.Slice(c, func(i, j int) bool {
		return CardLess(c[i],c[j])
	})
	return c[0]
}

func NewDeck() Deck {
	deck := make([]Card, 0, 54)
	suites := []rune{'s', 'd', 'c', 'h'}
	for _, s := range suites {
		for i := 0; i < 13; i++ {
			deck = append(deck, Card{Suit: s, Rank: uint8(i)})
		}
	}
	deck = append(deck, Card{
		Suit: 'r',
		Rank: 13,
	}, Card{
		Suit: 'b',
		Rank: 13,
	})
	return deck
}

func Shuffle(d Deck) Deck {
	rand.Shuffle(len(d), func(i, j int) {
		c := d[i]
		d[i] = d[j]
		d[j] = c
	})
	return d
}
func Deal(deck Deck) (Card, Deck) {
	if len(deck) <= 0 {
		return Card{
			Suit: 'n',
			Rank: 0,
		}, deck
	}
	cards,newDeck := DealN(deck,1)
	return cards[0], newDeck
}

func DealN(deck Deck, num int) ([]Card, Deck) {
	if len(deck) < num {
		panic("Handed deal an empty Deck")
	}
	cards := deck[len(deck)-num:]
	newDeck := deck[:len(deck)-num]
	return cards, newDeck
}


func CombineReshuffle(deck Deck,discard Deck) Deck {
	newDeck := append(deck,discard...)
	Shuffle(newDeck)
	return newDeck
}

func DealToN(deck Deck, n uint8) ([]Card, Deck) {
	cards := []Card{}
	for {
		var card Card
		card, deck = Deal(deck)
		cards = append(cards,card)
		if card.Rank >= n {
			return cards, deck
		}
	}
}


func DealToJoker(d Deck) int {
	for i, card := range d {
		if card.Suit == 'r' || card.Suit == 'b' {
			return i
		}
	}
	return -1
}

